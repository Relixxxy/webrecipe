# WebRecipe

## About
WebRecipe.Api it is an ASP.NET Core Web API application for client https://github.com/Zakotiuk/Hackathon-INT20H
## How to build/run
Prerequirements:
- docker installed.


1. Create PostgreSQL db server.

2. Build image:
```docker build -t web-recipe .```


3. Run container from created image:
```docker run -d -p 80:80 -e DB_CONNECTION_STRING="server={SERVER_IP_ADDR};port=5432;database={DB_NAME};uid={DB_USERNAME};password={DB_PASSWORD};" --name web-recipe1 web-recipe```


Now you can work with API on http://localhost/


4. To stop container run:
```docker container stop web-recipe1```
5. To remove container run:
```docker rm web-recipe1```

## Endpoints
All URLS require you to use POST method. 

https://api.junonian.earth/api/v1/Categories/GetDishCategories
```json
    - request: nothing
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "whiteIcon": "string",
                "blackIcon": "string"
            }
        ]
    }
```
https://api.junonian.earth/api/v1/Categories/GetProductsCategories
```json
    - request: nothing
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "whiteIcon": "string",
                "blackIcon": "string"
            }
        ]
    }
```
https://api.junonian.earth/api/v1/Dish/GetDishes
```json
    - request: nothing
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "recipe": "string",
                "difficulty": "string",
                "image": "string",
                "category": {
                    "id": 0,
                    "name": "string",
                    "whiteIcon": "string",
                    "blackIcon": "string"
                },
                "products": [
                    {
                        "id": 0,
                        "name": "string",
                        "image": "string",
                        "measure": "string",
                        "amount": 0,
                        "productId": 0,
                        "dishId": 0
                    }
                ]
            }
        ]
    }
```
https://api.junonian.earth/api/v1/Dish/GetDishesByProducts
```json
    - request: nothing
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "recipe": "string",
                "difficulty": "string",
                "image": "string",
                "category": {
                    "id": 0,
                    "name": "string",
                    "whiteIcon": "string",
                    "blackIcon": "string"
                },
                "products": [
                    {
                        "id": 0,
                        "name": "string",
                        "image": "string",
                        "measure": "string",
                        "amount": 0,
                        "productId": 0,
                        "dishId": 0
                    }
                ]
            }
        ]
    }
```
https://api.junonian.earth/api/v1/Dish/GetDishesByCategory
```json
    - request:
    {
        "name": "string"
    }
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "recipe": "string",
                "difficulty": "string",
                "image": "string",
                "category": {
                    "id": 0,
                    "name": "string",
                    "whiteIcon": "string",
                    "blackIcon": "string"
                },
                "products": [
                    {
                        "id": 0,
                        "name": "string",
                        "image": "string",
                        "measure": "string",
                        "amount": 0,
                        "productId": 0,
                        "dishId": 0
                    }
                ]
            }
        ]
    }
```
https://api.junonian.earth/api/v1/Dish/GetDishById
```json
    - request:
    {
        "id": 0
    }
    - response:
    {
        "id": 0,
        "name": "string",
        "recipe": "string",
        "difficulty": "string",
        "image": "string",
        "category": {
            "id": 0,
            "name": "string",
            "whiteIcon": "string",
            "blackIcon": "string"
        },
        "products": [
            {
                "id": 0,
                "name": "string",
                "image": "string",
                "measure": "string",
                "amount": 0,
                "productId": 0,
                "dishId": 0
            }
        ]
    }
```
https://api.junonian.earth/api/v1/Dish/GetDishByName
```json
    - request:
    {
        "name": "string"
    }
    - response:
    {
        "id": 0,
        "name": "string",
        "recipe": "string",
        "difficulty": "string",
        "image": "string",
        "category": {
            "id": 0,
            "name": "string",
            "whiteIcon": "string",
            "blackIcon": "string"
        },
        "products": [
            {
                "id": 0,
                "name": "string",
                "image": "string",
                "measure": "string",
                "amount": 0,
                "productId": 0,
                "dishId": 0
            }
        ]
    }
```
https://api.junonian.earth/api/v1/Dish/Add
```json
    - request:
    {
        "name": "string",
        "recipe": "string",
        "difficulty": "string",
        "image": "string",
        "categoryId": 0,
        "products": [
            {
                "productId": 0,
                "amount": 0
            }
        ]
    }
    - response:
    {
        "id": 0
    }
```
https://api.junonian.earth/api/v1/Products/GetAll
```json
    - request: nothing
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "image": "string",
                "measure": "string"
            }
        ]
    }
```
https://api.junonian.earth/api/v1/UserProducts/GetProducts
```json
    - request: nothing
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "image": "string",
                "measure": "string",
                "amount": 0,
                "category": {
                    "id": 0,
                    "name": "string",
                    "whiteIcon": "string",
                    "blackIcon": "string"
                }
            }
        ]
    }
```
https://api.junonian.earth/api/v1/UserProducts/GetMissingProducts
```json
    - request:
    {
        "products": [
            {
                "name": "string",
                "amount": 0
            }
        ]
    }
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "image": "string",
                "measure": "string",
                "amount": 0,
                "category": {
                    "id": 0,
                    "name": "string",
                    "whiteIcon": "string",
                    "blackIcon": "string"
                }
            }
        ]
    }
```
https://api.junonian.earth/api/v1/UserProducts/GetProductsByCategory
```json
    - request:
    {
        "name": "string"
    }
    - response:
    {
        "items": [
            {
                "id": 0,
                "name": "string",
                "image": "string",
                "measure": "string",
                "amount": 0,
                "category": {
                    "id": 0,
                    "name": "string",
                    "whiteIcon": "string",
                    "blackIcon": "string"
                }
            }
        ]
    }
```
## Infrustructure explained
Application is containeraized and managed using Portainer.  

CI/CD configured to build and push docker image to Docker Hub on commit where webhook is set to trigger Portainer Stack update.   

Application uses postgresql database and nginx as reverse proxy, hosted on AWS.  
